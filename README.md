
# Dice Poker

A dice poker game, an app to play with Typescript and PWA.

## React App Quick start

```shell
$ yarn
$ yarn start
>> http://localhost:3000/
```
