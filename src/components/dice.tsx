/**
 * Dice
 * Really one die, but that's a terrible name for a component
 */
import classNames from 'classnames';
import React from 'react';

type DiceProps = {
	id: number,
	onClick: Function,
	selected: boolean,
	value: number,
}

const Dice = ( { id, onClick, selected, value }: DiceProps ) => {
	const classes = classNames( 'dice', { selected: selected } );
	return (
		<div className={ classes } onClick={ () => onClick( id, !selected ) }> { value } </div>
	)
}

export default Dice;