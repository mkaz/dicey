/**
 * ScoreLine
 *
 */
import classNames from 'classnames';
import React from 'react';

type ScoreLineProps = {
	id: string,
	title: string,
	score: number,
	onClick: Function,
	placed?: boolean,
};

const ScoreLine = ( { id, onClick, placed, score, title  }: ScoreLineProps ) => {
	const classes = classNames( "scoreline", { placed: placed } );
	const isActive =  onClick && !placed;
	return (
		<div
			className={ classes }
			onClick={ isActive ? () => onClick( id, score ) : ()=>{} }
		>
			<span className="title">
				{ title }
			</span>
			<span className="score">
				{ score }
			</span>
		</div>
	);
}

export default ScoreLine;