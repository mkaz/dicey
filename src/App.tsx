/**
 * App
 */
import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import './App.css';
import Dice from './components/dice';
import ScoreLine from './components/score-line';
import calcScore from './lib/calc-score';
import useLocalStorage from './lib/use-local-storage';

const AppVersion = "v0.3.1";

const emptyScore: { [index:string] : number } = {
	'aces': -1,
	'twos': -1,
	'threes': -1,
	'fours': -1,
	'fives': -1,
	'sixes': -1,
	'threeKind': -1,
	'fourKind': -1,
	'fullHouse': -1,
	'smallStraight': -1,
	'largeStraight': -1,
	'yahtzee': -1,
	'chance': -1,
	'bonus': 0,
};


const App = () => {
	const [ dice, setDice ] = useState( [ 0, 0, 0, 0, 0 ] );
	const [ froze, setFroze ] = useState( [ false, false, false, false, false ] );

	const [ rollNum, setRollNumber ] =  useState( 0 );
	const [ roundNum, setRoundNumber ] = useState( 0 );

	// Score is the locked in score for the game
	// Transient score for each spot for the roll
	// Display is the score displayed, a mix of score/trans
	const [ score, setScore ] = useState( emptyScore );
	const [ trans, setTrans ] = useState( emptyScore );
	const [ display, setDisplay ] = useState( emptyScore );
	const [ finalScore, setFinalScore ] = useState( 0 );
	const [ highScore, setHighScore ] = useLocalStorage( 'highscore', 0 );

	const dieClick = ( die: number, selected: boolean ) => {
		const frozeArray = [ ...froze.slice(0, die), selected, ...froze.slice(die+1) ];
		setFroze( frozeArray );
	};

	const rollDice = () => {
		let newDice = [];
		for ( let i=0; i < 5; i++ ) {
			newDice[i] = ( froze[i] ) ? dice[i] : Math.floor((Math.random() * 6) + 1);
		}
		setDice( newDice );
		setTrans( calcScore( newDice ) );
		setRollNumber( rollNum + 1 );
	};

	const checkBonus = ( score: { [index:string] : number } ) => {
		if ( score.aces + score.twos + score.threes + score.fours + score.fives + score.sixes >= 63 ) {
			return 35;
		}
		return 0;
	}

	// When a person clicks the score
	const placeScore = ( key: string, val: number ) => {
		const s = Object.assign( {}, score );
		s[key] = val;
		// check bonus
		s.bonus = checkBonus(s);
		setScore( s );
		setDice( [ 0, 0, 0, 0, 0 ] );

		// set high score
		const sum = Object.values(s).reduce( sumScore, 0 );
		if ( sum > highScore ) {
			setHighScore(sum);
		}

		// reset
		setRollNumber( 0 );
		setFroze( [ false, false, false, false, false ] );
		setTrans( emptyScore );
		setRoundNumber( roundNum + 1 );
	}

	const sumScore = ( total: number, value: number ) => {
		return ( value >= 0 ) ? total + value : total;
	}

	const startNewGame = () => {
		setDice( [ 0, 0, 0, 0, 0 ] );
		setFroze( [ false, false, false, false, false ] );
		setRollNumber( 0 );
		setRoundNumber( 0 );
		setScore( emptyScore );
		setTrans( emptyScore );
		setDisplay( emptyScore );
		setFinalScore( 0 );
	}

	useEffect( () => {
		const s = Object.assign({}, emptyScore );
		Object.keys(score).forEach( ( key, idx ) => {
			const tv = trans[key] < 0 ? 0 : trans[key];
			s[key] = ( score[key] >= 0 ) ? score[key] : tv;
		});
		setDisplay(s);
		setFinalScore( Object.values(score).reduce( sumScore, 0 ) );
	}, [score, trans])

	const scoreClasses = classNames("summary", { final: roundNum >= 13 })
	return (
		<div className="App">
			<header className="App-header">
				<h2> Dice Poker </h2>
			</header>

			<main>
				<div className="board">
					<div className="rollButton">
						<button
							disabled={ rollNum >= 3 || roundNum >= 13 }
							onClick={ rollDice }
							className="button-roll"> Roll </button>
						<div> Roll: { rollNum } / 3 </div>
					</div>
					{ [0, 1, 2, 3, 4].map( ( num ) => (
						<Dice id={ num } onClick={ dieClick } selected={ froze[num] } value={ dice[num] } />
					))}
				</div>
				<div className="scorecard">
					<div className="leftSide">
						{ [ "aces", "twos", "threes", "fours", "fives", "sixes"].map( ( key ) => (
							<ScoreLine id={ key } onClick={ placeScore } title={ key } score={ display[key] } placed={ score[key] >= 0 && display[key] === score[key] } />
						))}
						<ScoreLine id="bonus" title="Bonus" score={ score.bonus } onClick={ () => {} } />
					</div>
					<div className="rightSide">
						<ScoreLine id="threeKind" onClick={ placeScore } title="Three of Kind" score={ display.threeKind }  placed={ score.threeKind >= 0 && display.threeKind === score.threeKind }/>
						<ScoreLine id="fourKind" onClick={ placeScore } title="Four of Kind" score={ display.fourKind } placed={ score.fourKind >= 0 && display.fourKind === score.fourKind } />
						<ScoreLine id="fullHouse" onClick={ placeScore } title="Full House" score={ display.fullHouse }  placed={ score.fullHouse >= 0 && display.fullHouse === score.fullHouse }/>
						<ScoreLine id="smallStraight" onClick={ placeScore } title="Small Straight" score={ display.smallStraight }  placed={ score.smallStraight >= 0 && display.smallStraight === score.smallStraight }/>
						<ScoreLine id="largeStraight" onClick={ placeScore } title="Large Straight" score={ display.largeStraight }  placed={ score.largeStraight >= 0 && display.largeStraight === score.largeStraight }/>
						<ScoreLine id="yahtzee" onClick={ placeScore } title="Yahtzee" score={ display.yahtzee }  placed={ score.yahtzee >= 0 && display.yahtzee === score.yahtzee }/>
						<ScoreLine id="chance" onClick={ placeScore } title="Chance" score={ display.chance }  placed={ score.chance >= 0 && display.chance === score.chance }/>
					</div>
				</div>
				<div className="scoreboard">
					<div className={ scoreClasses }>
						<div className="score">
							<label>Score:</label>
							<span className="value">{ finalScore } </span>
						</div>
						<div className="round">
							<label>Round:</label>
							<span className="value">{ roundNum } / 13 </span>
						</div>
						<div className="newgame">
							{ ( roundNum >= 13 ) && <button
								onClick={ startNewGame }
								className="button-new-game"> Start New Game
							</button> }
						</div>
					</div>
					<div className="topten">
						<table>
							<tr><td> High Score: </td><td>{ highScore } </td></tr>
							<tr><td> Games Played: </td><td> </td></tr>
							<tr><td> Avg Score: </td><td> </td></tr>
						</table>
					</div>
				</div>

			</main>
			<footer>
				Dicey { AppVersion }
			</footer>
		</div>
	);
}

export default App;
