import { isFullHouse, isSmallStraight, isLargeStraight } from './calc-score';

// isFullHouse does not check dice rolls but counts
test('isFullHouse ', () => {
	expect(isFullHouse([2, 0, 0, 0, 3])).toBeTruthy();
	expect(isFullHouse([0, 3, 2, 0, 0])).toBeTruthy();
	expect(isFullHouse([5, 0, 0, 0, 0])).toBeTruthy();
	expect(isFullHouse([0, 0, 0, 0, 5])).toBeTruthy();
	expect(isFullHouse([0, 3, 1, 1, 1])).toBeFalsy();
	expect(isFullHouse([2, 2, 1, 0, 0])).toBeFalsy();
});

test('isSmallStraight', () => {
	expect(isSmallStraight([1, 2, 3, 4, 5])).toBeTruthy();
	expect(isSmallStraight([1, 3, 4, 5, 6])).toBeTruthy();
	expect(isSmallStraight([2, 2, 5, 4, 3])).toBeTruthy();
	expect(isSmallStraight([2, 1, 3, 4, 6])).toBeTruthy();
	expect(isSmallStraight([4, 1, 3, 5, 4])).toBeFalsy();
	expect(isSmallStraight([4, 4, 3, 5, 4])).toBeFalsy();
	expect(isSmallStraight([4, 1, 2, 5, 6])).toBeFalsy();
});

test('isLargeStraight', () => {
	expect(isLargeStraight([4, 2, 3, 5, 1])).toBeTruthy();
	expect(isLargeStraight([1, 2, 3, 5, 4])).toBeTruthy();
	expect(isLargeStraight([1, 6, 3, 5, 4])).toBeFalsy();
	expect(isLargeStraight([4, 4, 3, 5, 4])).toBeFalsy();
});
