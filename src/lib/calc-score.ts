/**
 * Calculate score
 * @param dice Array<number> - an array of 5 numbers, the dice roll
 * @return score Object
 */
const calcScore = (dice: Array<number>) => {
	const score = {
		aces: 0,
		twos: 0,
		threes: 0,
		fours: 0,
		fives: 0,
		sixes: 0,
		threeKind: 0,
		fourKind: 0,
		fullHouse: 0,
		smallStraight: 0,
		largeStraight: 0,
		yahtzee: 0,
		chance: 0,
	};
	const counts = [
		dice.filter((d) => d === 1).length,
		dice.filter((d) => d === 2).length,
		dice.filter((d) => d === 3).length,
		dice.filter((d) => d === 4).length,
		dice.filter((d) => d === 5).length,
		dice.filter((d) => d === 6).length,
	];

	score.aces = 1 * counts[0];
	score.twos = 2 * counts[1];
	score.threes = 3 * counts[2];
	score.fours = 4 * counts[3];
	score.fives = 5 * counts[4];
	score.sixes = 6 * counts[5];

	if (counts.reduce((agg, val) => agg || val >= 3, false)) {
		// three of a kind, sum all the dice
		score.threeKind = dice.reduce((total, amount) => total + amount);
	}

	if (counts.reduce((agg, val) => agg || val >= 4, false)) {
		// four of a kind, sum all the dice
		score.fourKind = dice.reduce((total, amount) => total + amount);
	}

	// calculate full house
	if (isFullHouse(counts)) {
		score.fullHouse = 25;
	}

	// calculate straights
	if (isSmallStraight(dice)) {
		score.smallStraight = 30;
	}

	if (isLargeStraight(dice)) {
		score.largeStraight = 40;
	}

	// yahtzee
	if (counts.reduce((agg, val) => agg || val === 5, false)) {
		score.yahtzee = 50;
		// todo: yahtzee bonus ?
	}

	score.chance = dice.reduce((total, amount) => total + amount);

	return score;
};

export const isFullHouse = (counts: Array<number>) => {
	if (counts.includes(3) && counts.includes(2)) {
		return true;
	}
	if (counts.includes(5)) {
		return true;
	}
	return false;
};

export const isSmallStraight = (dice: Array<number>) => {
	// de-duplicate
	const unique = dice.filter(function (el, i, arr) {
		return arr.indexOf(el) === i;
	});

	if (unique.length < 4) {
		return false;
	}

	// sort them in order and then they must be consecutive
	// walk through confirm each one is only one higher
	unique.sort();
	let count = 0;
	for (let i = 0; i < unique.length - 1; i++) {
		if (unique[i] + 1 === unique[i + 1]) {
			count++;
		} else {
			count = 0; // invalid, reset
		}
		if (count >= 3) {
			return true;
		}
	}
	// only check to 3 since last element is not checked
	// but has to be true since previous item is checked
	return count >= 3;
};

export const isLargeStraight = (dice: Array<number>) => {
	const unique = dice.filter(function (el, i, arr) {
		return arr.indexOf(el) === i;
	});

	if (unique.length < 5) {
		return false;
	}

	// sort them in order and then they must be consecutive
	// walk through confirm each one is only one higher
	unique.sort();
	for (let i = 0; i < unique.length - 1; i++) {
		if (unique[i] + 1 !== unique[i + 1]) {
			return false;
		}
	}
	return true;
};

export default calcScore;
